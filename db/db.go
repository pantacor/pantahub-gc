//
// Copyright 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package db

import (
	"context"
	"log"
	"os"
	"sync"
	"testing"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var once sync.Once

var (
	clientInstance *mongo.Client
)

// Client : function to get Mongo Client
func Client() *mongo.Client {

	once.Do(func() { // <-- atomic, does not allow repeating
		clientInstance = Connect()
	})

	return clientInstance
}

// Connect : To connect to the mongoDb
func Connect() *mongo.Client {
	//mongo-go-driver settings
	mongoClient, err := GetMongoClient()
	if err != nil {
		panic(err)
	}
	return mongoClient
}

// ConnectTest : To connect to the mongoDb for test
func ConnectTest(t *testing.T) *mongo.Client {
	//mongo-go-driver settings
	mongoClient, err := GetMongoClient()
	if err != nil {
		t.Error(err)
	}
	return mongoClient
}

// GetPantahubBaseDB : Get Pantahub Base DB
func GetPantahubBaseDB() string {
	return os.Getenv("MONGO_DB")
}

// GetDevSummaryDb : Get Dev Summary Db
func GetDevSummaryDb() string {
	return "pantabase_devicesummary"
}

// GetMongoClient : To Get Mongo Client Object
func GetMongoClient() (*mongo.Client, error) {
	MongoDb := os.Getenv("MONGO_DB")
	user := os.Getenv("MONGO_USER")
	pass := os.Getenv("MONGO_PASS")
	host := os.Getenv("MONGO_HOST")
	port := os.Getenv("MONGO_PORT")
	mongoRs := os.Getenv("MONGO_RS")

	//Setting Client Options
	clientOptions := options.Client()
	mongoConnect := "mongodb://"
	if user != "" {
		mongoConnect += user
		if pass != "" {
			mongoConnect += ":"
			mongoConnect += pass
		}
		mongoConnect += "@"
	}
	mongoConnect += host

	if port != "" {
		mongoConnect += ":"
		mongoConnect += port
	}

	mongoConnect += "/?"

	if user != "" {
		mongoConnect += "authSource=" + MongoDb
		mongoConnect += "&authMechanism=SCRAM-SHA-1"
	}

	if mongoRs != "" {
		mongoConnect += "&replicaSet=" + mongoRs
	}

	clientOptions = clientOptions.ApplyURI(mongoConnect)
	if mongoRs != "" {
		clientOptions = clientOptions.SetReplicaSet(mongoRs)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	log.Println("Will connect to mongo PROD db with: " + mongoConnect)
	client, err := mongo.Connect(ctx, clientOptions)

	return client, err
}
