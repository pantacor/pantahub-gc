//
// Copyright 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package routes

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"time"

	"gitlab.com/pantacor/pantahub-gc/db"
	"gopkg.in/mgo.v2/bson"
)

// HealthResponse : Health Response
type HealthResponse struct {
	Status        bool          `json:"status"`
	StatusMessage string        `json:"status_message"`
	ErrorCode     int           `json:"code"`
	Duration      time.Duration `json:"duration"`
	Start         time.Time     `json:"start-time"`
	Errors        []string      `json:"errors"`
}

// Health : handler function for GET /health call
func Health(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	response := HealthResponse{}
	response.Status = true
	response.StatusMessage = "SUCCESS"
	response.ErrorCode = 0
	response.Start = time.Now()

	// check DB
	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_devices")
	if collection == nil {
		response.Status = false
		response.Errors = append(response.Errors, "Error with Database connectivity")
	}
	val := map[string]interface{}{}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	err := collection.FindOne(ctx, bson.M{}).Decode(&val)
	if err != nil {
		response.Status = false
		response.Errors = append(response.Errors, "Error with Database query:"+err.Error())
	}

	end := time.Now()
	response.Duration = end.Sub(response.Start)

	if !response.Status {
		response.StatusMessage = "FAIL"
		response.ErrorCode = 1
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
	}

	b, _ := json.MarshalIndent(response, "", "    ")
	log.Print("/health Response:\n" + string(b))

	json.NewEncoder(w).Encode(response)
}
