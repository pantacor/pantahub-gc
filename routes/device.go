//
// Copyright 2018  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package routes

import (
	"encoding/json"
	"log"
	"net/http"
	"os"

	"gitlab.com/pantacor/pantahub-gc/models"
)

// MarkDeviceAsGarbage : Mark a device as garbage
func MarkDeviceAsGarbage(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	if os.Getenv("DEBUG") == "true" {
		log.Println("Inside PUT /markgarbage/device/{id} Handler")
	}

	device := &models.Device{}

	err := device.Validate(r)
	if err != nil {

		w.WriteHeader(http.StatusBadRequest)
		response := map[string]interface{}{
			"status": 0,
			"errors": err.Error(),
		}
		json.NewEncoder(w).Encode(response)

	} else {
		_, err := device.MarkDeviceAsGarbage()

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			response := map[string]interface{}{
				"status": 0,
				"errors": err.Error(),
			}
			json.NewEncoder(w).Encode(response)
		} else {
			w.WriteHeader(http.StatusOK)
			response := map[string]interface{}{
				"status":  1,
				"message": "Device marked as garbage",
				"device":  device,
			}
			json.NewEncoder(w).Encode(response)
		}

	}

}

// MarkUnClaimedDevicesAsGarbage : Mark all unclaimed devices as garbage after a while(eg: after 5 days)
func MarkUnClaimedDevicesAsGarbage(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if os.Getenv("DEBUG") == "true" {
		log.Println("Inside PUT /markgarbage/devices/unclaimed Handler")
	}
	device := &models.Device{}
	response, err := device.MarkUnClaimedDevicesAsGarbage()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
	}
	json.NewEncoder(w).Encode(response)
}

// ProcessDeviceGarbages : Find all device documents with gc_processed=false then mark it associated trail as garbages
func ProcessDeviceGarbages(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if os.Getenv("DEBUG") == "true" {
		log.Println("Inside PUT /processgarbages/device Handler")
	}
	device := &models.Device{}
	processResponse, err := device.ProcessDeviceGarbages()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
	}
	json.NewEncoder(w).Encode(processResponse)
}

// DeleteDeviceGarbages : Delete Garbages of a Device
func DeleteDeviceGarbages(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if os.Getenv("DEBUG") == "true" {
		log.Println("Inside DeletekDeviceGarbages")
	}
	device := &models.Device{}
	response, err := device.DeleteGarbages()

	if response.StatusMessage == "DISABLED" {
		w.WriteHeader(http.StatusNotImplemented)
	} else if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
	}
	json.NewEncoder(w).Encode(response)
}

// MarkGarbagesInDeviceSummary : Mark Garbages (Devices which are deleted) In DeviceSummary (Db:pantabase_devicesummary,collection:device_summary_short_new_v2)
func MarkGarbagesInDeviceSummary(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if os.Getenv("DEBUG") == "true" {
		log.Println("Inside PUT /markgarbage/devicesummary Handler")
	}
	device := &models.Device{}
	response, err := device.MarkDeviceSummaryGarbages()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
	}
	json.NewEncoder(w).Encode(response)
}
