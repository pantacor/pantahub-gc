//
// Copyright 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package routes

import (
	"encoding/json"
	"log"
	"net/http"
	"net/url"

	"gitlab.com/pantacor/pantahub-gc/models"
)

// CronResponse : Response of PUT /cron
type CronResponse struct {
	Status                         int                                      `json:"status"`
	StatusMessage                  string                                   `json:"status_message"`
	Errors                         []error                                  `json:"errors"`
	DeleteDeviceGarbages           models.DeleteGarbagesResponse            `json:"delete_device_garbages"`
	MarkTrailGarbages              models.MarkTrailGarbagesResponse         `json:"mark_trail_garbages"`
	MarkUnclaimedDevicesAsGarbages models.MarkUnclaimedDevicesResponse      `json:"mark_unclaimed_devices_as_garbages"`
	PopulateStepsUsedObjects       models.PopulateStepsUsedObjectsResponse  `json:"populate_steps_used_objects"`
	PopulateTrailsUsedObjects      models.PopulateTrailsUsedObjectsResponse `json:"populate_trails_used_objects"`
	ProcessDeviceGarbages          models.ProcessDeviceGarbagesResponse     `json:"process_device_garbages"`
	ProcessTrailGarbages           models.ProcessTrailGarbagesResponse      `json:"process_trail_garbages"`
	ProcessStepGarbages            models.ProcessStepGarbagesResponse       `json:"process_step_garbages"`
}

// Cron : handler function for GET /cron call
func Cron(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	log.Print("Inside Cron job Handler: PUT /cron")

	response := CronResponse{
		Status:        1,
		StatusMessage: "SUCCESS",
		Errors:        []error{},
	}
	//Populate trails used objects
	trail := &models.Trail{}
	var err error
	response.PopulateTrailsUsedObjects, err = trail.PopulateAllTrailsUsedObjects()
	if err != nil {
		log.Print("Failed populate/usedobjects/trails : Populate trails used objects")
		log.Print(err.Error())
		response.Status = 0
		response.StatusMessage = "FAIL"
		response.Errors = append(response.Errors, err)
	}
	// Populate steps used objects
	step := &models.Step{}
	response.PopulateStepsUsedObjects, err = step.PopulateAllStepsUsedObjects()
	if err != nil {
		log.Print("Failed PUT populate/usedobjects/steps : Populate steps used objects")
		log.Print(err.Error())
		response.Status = 0
		response.StatusMessage = "FAIL"
		response.Errors = append(response.Errors, err)
	}
	// Mark Unclaimed Devices as Garbage
	device := &models.Device{}
	response.MarkUnclaimedDevicesAsGarbages, err = device.MarkUnClaimedDevicesAsGarbage()
	if err != nil {
		log.Print("Failed PUT /markgarbage/devices/unclaimed : Mark Unclaimed Devices as Garbage")
		log.Print(err.Error())
		response.Status = 0
		response.StatusMessage = "FAIL"
		response.Errors = append(response.Errors, err)
	}

	// Mark Trail as Garbage that lost their parent device
	response.MarkTrailGarbages, err = models.MarkAllTrailGarbages()
	if err != nil {
		log.Print("Failed PUT /markgarbage/trails : Mark Trail as Garbage that lost their parent device")
		log.Print(err.Error())
		response.Status = 0
		response.StatusMessage = "FAIL"
		response.Errors = append(response.Errors, err)
	}
	// Process Device Garbages
	device = &models.Device{}
	response.ProcessDeviceGarbages, err = device.ProcessDeviceGarbages()
	if err != nil {
		log.Print("Failed PUT /processgarbages/devices : Process Device Garbages")
		log.Print(err.Error())
		response.Status = 0
		response.StatusMessage = "FAIL"
		response.Errors = append(response.Errors, err)
	}
	// Process Trail Garbages
	trail = &models.Trail{}
	response.ProcessTrailGarbages, err = trail.ProcessTrailGarbages()
	if err != nil {
		log.Print("Failed PUT /processgarbages/trails : Process Trail Garbages")
		log.Print(err.Error())
		response.Status = 0
		response.StatusMessage = "FAIL"
		response.Errors = append(response.Errors, err)
	}
	// Process Step Garbages
	step = &models.Step{}
	response.ProcessStepGarbages, err = step.ProcessStepGarbages()
	if err != nil {
		log.Print("Failed PUT /processgarbages/steps : Process Step Garbages")
		log.Print(err.Error())
		response.Status = 0
		response.StatusMessage = "FAIL"
		response.Errors = append(response.Errors, err)
	}

	// DELETE device Garbages
	device = &models.Device{}
	response.DeleteDeviceGarbages, err = device.DeleteGarbages()
	if err != nil {
		log.Print("Failed DELETE /devices : DELETE device Garbages")
		log.Print(err.Error())
		response.Status = 0
		response.StatusMessage = "FAIL"
		response.Errors = append(response.Errors, err)
	}

	if response.Status == 1 {
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusInternalServerError)
	}
	log.Print("Response of PUT /cron")
	logResponse(response)
	json.NewEncoder(w).Encode(response)
}
func logErrors(errs url.Values) {
	log.Print("Errors:")
	log.Print(errs)
}
func logResponse(res CronResponse) {
	b, _ := json.MarshalIndent(res, "", "    ")
	log.Print(string(b))
}
