//
// Copyright 2018-2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package models

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"log"
	"os"
	"strconv"
	"strings"
	time "time"

	"gitlab.com/pantacor/pantahub-gc/db"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"
)

// Trail : Trail model
type Trail struct {
	ID     primitive.ObjectID `json:"id" bson:"_id"`
	Owner  string             `json:"owner"`
	Device string             `json:"device"`
	//  Admins   []string `json:"admins"`   // XXX: maybe this is best way to do delegating device access....
	LastInSync       time.Time              `json:"last-insync" bson:"last-insync"`
	LastTouched      time.Time              `json:"last-touched" bson:"last-touched"`
	FactoryState     map[string]interface{} `json:"factory-state" bson:"factory-state"`
	Garbage          bool                   `json:"garbage" bson:"garbage"`
	GarbageRemovalAt time.Time              `bson:"garbage_removal_at" json:"garbage_removal_at"`
	GcProcessed      bool                   `json:"gc_processed" bson:"gc_processed"`
	UsedObjects      []string               `bson:"used_objects" json:"used_objects"`
}

// ProcessTrailGarbagesResponse : response of ProcessTrailGarbages() function
type ProcessTrailGarbagesResponse struct {
	Status                 int      `json:"status"`
	StatusMessage          string   `json:"status_message"`
	TrailsProcessed        int      `json:"trails_processed"`
	StepsMarkedAsGarbage   int      `json:"steps_marked_as_garbage"`
	ObjectsMarkedAsGarbage int      `json:"objects_marked_as_garbage"`
	TrailsWithErrors       int      `json:"trails_with_errors"`
	ObjectsWithErrors      int      `json:"objects_with_errors"`
	ObjectsIgnored         int      `json:"objects_ignored"`
	StepsWithErrors        int      `json:"steps_with_errors"`
	Warnings               []string `json:"warnings"`
}

// PopulateUsedObjectsResponse : Populate UsedObject Response
type PopulateUsedObjectsResponse struct {
	Status                   int      `json:"status"`
	StatusMessage            string   `json:"status_message"`
	ObjectList               []string `json:"object_list"`
	InvalidObjectCount       int      `json:"invalid_object_count"`
	InvalidState             bool     `json:"invalid_state"`
	ObjectsUnMarkedAsGarbage []string `json:"objects_unmarked_as_garbage"`
	Warnings                 []string `json:"warnings"`
}

// PopulateTrailsUsedObjectsResponse : Populate Trails Used Objects Response
type PopulateTrailsUsedObjectsResponse struct {
	Status                   int      `json:"status"`
	StatusMessage            string   `json:"status_message"`
	TrailsPopulated          int      `json:"trails_populated"`
	TrailsWithErrors         int      `json:"trails_with_errors"`
	ObjectsUnMarkedAsGarbage []string `json:"objects_unmarked_as_garbage"`
	Warnings                 []string `json:"warnings"`
}

// MarkTrailGarbagesResponse : response of MarkAllTrailGarbages() function
type MarkTrailGarbagesResponse struct {
	Status        int      `json:"status"`
	StatusMessage string   `json:"status_message"`
	TrailsMarked  int      `json:"trails_marked"`
	Warnings      []string `json:"warnings"`
}

// DeleteTrailsResponse : response of deleteTrailGarbage() function
type DeleteTrailsResponse struct {
	Status        int      `json:"status"`
	StatusMessage string   `json:"status_message"`
	TrailsRemoved int      `json:"trails_removed"`
	Warnings      []string `json:"warnings"`
}

// MarkAllTrailGarbages : Mark all trail garbages
func MarkAllTrailGarbages() (
	response MarkTrailGarbagesResponse,
	err error,
) {
	response.TrailsMarked = 0

	trailsWithErrors := []string{}

	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_trails")
	ctx := context.Background()
	findOptions := options.Find()
	findOptions.SetNoCursorTimeout(true)
	/* Note: Actual Record fetching will not happen here
	as it is using mongodb cursor and record fetching will
	start with we call cur.Next() */
	// Create mongo cursor
	cur, err := collection.Find(ctx, bson.M{
		"garbage": bson.M{"$ne": true}}, findOptions)
	if err != nil {
		return response, errors.New("Error fetching trails:" + err.Error())
	}
	if cur != nil {
		defer cur.Close(ctx)
	}
	for cur != nil && cur.Next(ctx) {
		err := cur.Err()
		if err != nil {
			return response, errors.New("Cursor error:" + err.Error())
		}
		trail := Trail{}
		err = cur.Decode(&trail)
		if err != nil {
			response.Warnings = append(response.Warnings, "Cursor decode error:"+err.Error())
			continue
		}
		if contains(trailsWithErrors, trail.ID.Hex()) {
			continue
		}
		deviceResult, err := IsDeviceValid(trail.ID)
		if err != nil {
			response.Warnings = append(response.Warnings, err.Error())
			trailsWithErrors = append(trailsWithErrors, trail.ID.Hex())
		}
		if !deviceResult {
			warnings, err := markTrailAsGarbage(trail.ID)
			if err != nil {
				response.Warnings = append(response.Warnings, err.Error())
				continue
			}
			if len(warnings) > 0 {
				response.Warnings = append(response.Warnings, warnings...)
				continue
			}
			response.TrailsMarked++
		}
	}

	if err := cur.Err(); err != nil {
		response.Warnings = append(response.Warnings, "Cursor error:"+err.Error())
	}

	response.Status = 1
	response.StatusMessage = "SUCCESS"
	return response, err
}

// markTrailAsGarbage : Mark Trail A sGarbage
func markTrailAsGarbage(deviceID primitive.ObjectID) (
	warnings []string,
	err error,
) {

	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_trails")
	GarbageRemovalAt, err := GetGarbageRemovalTime()
	if err != nil {
		return warnings, err
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	updateResult, err := collection.UpdateOne(
		ctx,
		bson.M{"_id": deviceID},
		bson.M{"$set": bson.M{
			"garbage":            true,
			"garbage_removal_at": GarbageRemovalAt,
			"gc_processed":       false,
		}},
	)
	if err != nil {
		return warnings, errors.New("Error marking trail as garbage:" + err.Error() + "[Trail ID:" + deviceID.Hex() + "]")
	}
	if updateResult.MatchedCount == 0 {
		warnings = append(warnings, "Warning on marking trail as garbage:Invalid trail [Trail ID:"+deviceID.Hex()+"]")
		return warnings, err
	}

	trail := Trail{}
	ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	err = collection.FindOne(ctx,
		bson.M{"_id": deviceID}).
		Decode(&trail)
	if err != nil {
		warnings = append(warnings, "Error fetching trail:"+err.Error())
		return warnings, err
	}
	// keep used_objects of trails as upto date
	_, err = trail.populateTrailUsedObjects()
	if err != nil {
		return warnings, err
	}
	// keep used_objects of step(rev=0) as upto date
	step := Step{}
	step.ID = trail.ID.Hex() + "-0"
	step.Owner = trail.Owner
	step.State = trail.FactoryState
	_, err = step.populateStepsUsedObjects()
	if err != nil {
		return warnings, err
	}

	return warnings, err
}

// ProcessTrailGarbages : Process Trail Garbages
func (trail *Trail) ProcessTrailGarbages() (
	response ProcessTrailGarbagesResponse,
	err error,
) {
	response.Status = 0
	response.StatusMessage = "FAIL"

	response.TrailsProcessed = 0
	response.StepsMarkedAsGarbage = 0
	response.ObjectsMarkedAsGarbage = 0
	response.TrailsWithErrors = 0
	response.ObjectsWithErrors = 0
	response.ObjectsIgnored = 0
	response.StepsWithErrors = 0

	objectsIgnored := []string{}
	objectsMarkedAsGarbageList := []string{}
	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_trails")
	ctx := context.Background()
	findOptions := options.Find()
	findOptions.SetNoCursorTimeout(true)
	// Fetch all trail documents with (garbage:true AND (gc_processed:false if exist OR gc_processed not exist ))
	/* Note: Actual Record fetching will not happen here
	as it is using mongodb cursor and record fetching will
	start with we call cur.Next() */
	// Create mongo cursor
	cur, err := collection.Find(ctx, bson.M{
		"garbage":      true,
		"gc_processed": bson.M{"$ne": true},
	}, findOptions)
	if err != nil {
		return response, errors.New("Error fetching trails:" + err.Error())
	}
	if cur != nil {
		defer cur.Close(ctx)
	}
	for i := 0; cur != nil && cur.Next(ctx); i++ {
		err := cur.Err()
		if err != nil {
			return response, errors.New("Cursor error:" + err.Error())
		}
		trail := Trail{}
		err = cur.Decode(&trail)
		if err != nil {
			response.Warnings = append(response.Warnings, "Cursor decode error:"+err.Error())
			continue
		}
		if os.Getenv("DEBUG") == "true" {
			log.Print("Processing Trail:" + strconv.Itoa(i) + "[ID:" + trail.ID.Hex() + "]")
		}

		populateResponse, err := trail.populateTrailUsedObjects()
		if len(populateResponse.Warnings) > 0 {
			response.Warnings = append(response.Warnings, populateResponse.Warnings...)
		}
		if err != nil {
			if os.Getenv("DEBUG") == "true" {
				log.Print("Found Errors in Trail while populating used_objects:" + strconv.Itoa(i) + "[ID:" + trail.ID.Hex() + "]")
				log.Print(err.Error())
			}
			response.Warnings = append(response.Warnings, err.Error())
			response.TrailsWithErrors++

			if populateResponse.InvalidObjectCount > 0 {
				response.ObjectsWithErrors += populateResponse.InvalidObjectCount
			}

			response.StepsWithErrors++ //for steps with rev=0
		}

		//1.Mark Steps As Garbage
		stepsMarked, err := markStepsAsGarbage(trail.ID)
		if err != nil {
			response.Warnings = append(response.Warnings, err.Error())
			//response.StepsWithErrors++
		}
		response.StepsMarkedAsGarbage += stepsMarked

		//2.Mark Objects As Garbage
		_,
			objectsMarkedList,
			objectsWithErrors,
			newObjectsIgnoredList,
			_,
			warnings,
			err := markObjectsAsGarbage(trail.UsedObjects)
		if err != nil {
			response.Warnings = append(response.Warnings, err.Error())
		}
		if len(warnings) > 0 {
			response.Warnings = append(response.Warnings, warnings...)
		}

		response.ObjectsWithErrors += objectsWithErrors

		/* Populating objects Marked  list,Note: Purpose of
		these 2 below for loops are to avoid counts of duplicate marked/ignored
		objects count when there is same object is used in multiple trails/steps */
		for _, v := range objectsMarkedList {
			if !contains(objectsMarkedAsGarbageList, v) {
				objectsMarkedAsGarbageList = append(objectsMarkedAsGarbageList, v)
			}
		}
		//populating igonored object list
		for _, v := range newObjectsIgnoredList {
			if !contains(objectsIgnored, v) {
				objectsIgnored = append(objectsIgnored, v)
			}
		}

		//Marking trail as gc_processed=true
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		updateResult, err := collection.UpdateOne(
			ctx,
			bson.M{"_id": trail.ID},
			bson.M{"$set": bson.M{"gc_processed": true}},
		)
		if err != nil {
			if os.Getenv("DEBUG") == "true" {
				log.Print("Error Marking Trail:" + strconv.Itoa(i) + " As Processed [ID:" + trail.ID.Hex() + "]")
			}
			return response, errors.New("Error marking trail as gc processed:" + err.Error() + "[ID:" + trail.ID.Hex() + "]")
		}
		if updateResult.ModifiedCount == 1 {
			response.TrailsProcessed++
			if os.Getenv("DEBUG") == "true" {
				log.Print("Marked Trail:" + strconv.Itoa(i) + " As Processed [ID:" + trail.ID.Hex() + "]")
			}
		}
	} //end for loop

	//populate Marked Objects Count
	response.ObjectsMarkedAsGarbage += len(objectsMarkedAsGarbageList)
	//Populating warning messages for ignored objects
	if len(objectsIgnored) > 0 {
		for _, v := range objectsIgnored {
			response.Warnings = append(response.Warnings, "Ignoring trail Object storage-id(_id):"+v+" due to more than 0 usage")
		}
	}
	//populate Ignored Objects Count
	response.ObjectsIgnored = len(objectsIgnored)

	response.Status = 1
	response.StatusMessage = "SUCCESS"
	return response, err
}

// populateTrailUsedObjects : Populate used_objects field in trail collection
func (trail *Trail) populateTrailUsedObjects() (
	response PopulateUsedObjectsResponse,
	err error,
) {
	response.Status = 0
	response.StatusMessage = "FAIL"

	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_trails")

	response, err = parseStateObjects(trail.Owner, trail.FactoryState, "Trail ID:"+trail.ID.Hex())
	if err != nil {
		return response, err
	}

	if response.InvalidState {

		if os.Getenv("DEBUG") == "true" {
			log.Print("Not Populated used_objects List for trail id:" + trail.ID.Hex())
			log.Print(response)
			log.Print("\n")
		}
		return response, errors.New("Invalid state found for trail id:" + trail.ID.Hex())
	}

	if response.InvalidObjectCount > 0 {

		if os.Getenv("DEBUG") == "true" {
			log.Print("Not Populated used_objects List for trail id:" + trail.ID.Hex())
			log.Print(response)
			log.Print("\n")
		}
		return response, errors.New("Invalid objects found for trail id:" + trail.ID.Hex())
	}

	if os.Getenv("DEBUG") == "true" {
		log.Print("Populated " + strconv.Itoa(len(trail.UsedObjects)) + " Used Objects for trail id:" + trail.ID.Hex())
		log.Print("\n")
	}

	trail.UsedObjects = response.ObjectList

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_, err = collection.UpdateOne(
		ctx,
		bson.M{"_id": trail.ID},
		bson.M{"$set": bson.M{
			"used_objects": response.ObjectList,
		}},
	)
	if err != nil {
		if os.Getenv("DEBUG") == "true" {
			log.Print("Not Populated used_objects List for trail id:" + trail.ID.Hex())
			log.Print(response)
			log.Print("\n")
		}
		return response, errors.New("Error updating trail used objects:" + err.Error() + "[ID:" + trail.ID.Hex() + "]")
	}

	if os.Getenv("DEBUG") == "true" {
		log.Print("Populated " + strconv.Itoa(len(trail.UsedObjects)) + " Used Objects for trail id:" + trail.ID.Hex())
		log.Print("\n")
	}

	response.Status = 1
	response.StatusMessage = "SUCCESS"

	return response, err
}

// parseStateObjects: Parse State Objects and populate ObjectList
func parseStateObjects(
	owner string,
	state map[string]interface{},
	model string,
) (
	response PopulateUsedObjectsResponse,
	err error,
) {
	response.Status = 0
	response.StatusMessage = "FAIL"

	response.ObjectList = []string{}
	response.InvalidObjectCount = 0
	response.ObjectsUnMarkedAsGarbage = []string{}
	response.Status = 0
	response.InvalidState = false

	objMap := map[string]bool{}

	spec, ok := state["#spec"]
	if !ok {
		response.Warnings = append(response.Warnings, "Invalid state:#spec is missing ["+model+"]")
		response.InvalidState = true
		return response, err
	}

	specValue, ok := spec.(string)
	if !ok {
		response.Warnings = append(response.Warnings, "Invalid state:Value of #spec should be string ["+model+"]")
		response.InvalidState = true
		return response, err
	}

	if specValue != "pantavisor-multi-platform@1" &&
		specValue != "pantavisor-service-system@1" {
		response.Warnings = append(response.Warnings, "Invalid state:Value of #spec should not be "+specValue+" ["+model+"]")
		response.InvalidState = true
		return response, err
	}

	for key, v := range state {
		if strings.HasSuffix(key, ".json") ||
			strings.HasSuffix(key, "Ｎjson") ||
			key == "#spec" {
			continue
		}
		sha, found := v.(string)
		if !found {
			response.Warnings = append(response.Warnings, "Object is not a string[sha:"+sha+","+model+"]")
			response.InvalidObjectCount++
			continue
		}
		shaBytes, err := DecodeSha256HexString(sha)
		if err != nil {
			response.Warnings = append(response.Warnings, "Object sha that could not be decoded from hex:"+err.Error()+" [sha:"+sha+","+model+"]")
			response.InvalidObjectCount++
			continue
		}
		// lets use proper storage shas to reflect that fact that each
		// owner has its own copy of the object instance on DB side
		storageSha := MakeStorageID(owner, shaBytes)
		result, _ := IsObjectValid(storageSha)
		if !result {
			response.Warnings = append(response.Warnings, "Object sha is not found in the db[storage-id(_id):"+storageSha+","+model+"]")
			response.InvalidObjectCount++
			continue
		}
		if _, ok := objMap[storageSha]; !ok {
			result, err := IsObjectGarbage(storageSha)
			if err != nil {
				return response, err
			}
			if result {
				err := UnMarkObjectAsGarbage(storageSha)
				if err != nil {
					return response, err
				}
				//Populate UnMarked Objects As Garbage List
				response.ObjectsUnMarkedAsGarbage = append(response.ObjectsUnMarkedAsGarbage, storageSha)

			}
			objMap[storageSha] = true
			response.ObjectList = append(response.ObjectList, storageSha)
		}
	}
	if response.InvalidObjectCount > 0 {
		return response, err
	}
	response.Status = 1
	response.StatusMessage = "SUCCESS"
	return response, err
}

// deleteTrailGarbage : Delete Trail Garbage
func deleteTrailGarbage() (
	response DeleteTrailsResponse,
	err error,
) {
	response.Status = 0
	response.StatusMessage = "FAIL"

	response.TrailsRemoved = 0

	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_trails")
	now := time.Now().Local()
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	info, err := collection.DeleteMany(ctx,
		bson.M{
			"garbage":            true,
			"garbage_removal_at": bson.M{"$lt": now},
		})
	if err != nil {
		if info != nil {
			response.TrailsRemoved = int(info.DeletedCount)
		}
		return response, errors.New("Error removing garbage trails:" + err.Error())
	}
	response.Status = 1
	response.StatusMessage = "SUCCESS"
	response.TrailsRemoved = int(info.DeletedCount)
	return response, err
}

// PopulateAllTrailsUsedObjects : Populate used_objects_field for all trails
func (trail *Trail) PopulateAllTrailsUsedObjects() (
	response PopulateTrailsUsedObjectsResponse,
	err error,
) {
	response.Status = 0
	response.StatusMessage = "FAIL"
	response.ObjectsUnMarkedAsGarbage = []string{}
	response.TrailsPopulated = 0
	response.TrailsWithErrors = 0

	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_trails")
	ctx := context.Background()
	findOptions := options.Find()
	findOptions.SetNoCursorTimeout(true)
	// Fetch all trail documents without used_objects field
	/* Note: Actual Record fetching will not happen here
	as it is using mongodb cursor and record fetching will
	start with we call cur.Next() */
	// Create mongo cursor
	cur, err := collection.Find(ctx, bson.M{
		"used_objects": bson.M{"$exists": false},
	}, findOptions)
	if err != nil {
		return response, errors.New("Error fetching trails:" + err.Error())
	}
	if cur != nil {
		defer cur.Close(ctx)
	}
	for cur != nil && cur.Next(ctx) {
		err := cur.Err()
		if err != nil {
			return response, errors.New("Cursor error:" + err.Error())
		}
		trail := Trail{}
		err = cur.Decode(&trail)
		if err != nil {
			response.Warnings = append(response.Warnings, "Cursor decode error:"+err.Error())
			continue
		}
		populateResponse, err := trail.populateTrailUsedObjects()
		if err != nil {
			response.Warnings = append(response.Warnings, err.Error())
			response.TrailsWithErrors++
		}
		if len(populateResponse.Warnings) > 0 {
			response.Warnings = append(response.Warnings, populateResponse.Warnings...)
		}
		if populateResponse.InvalidObjectCount == 0 && !populateResponse.InvalidState {
			response.TrailsPopulated++
		}
		//Merge  Objects UnMarked As Garbage
		response.ObjectsUnMarkedAsGarbage = append(response.ObjectsUnMarkedAsGarbage, populateResponse.ObjectsUnMarkedAsGarbage...)
	}

	response.Status = 1
	response.StatusMessage = "SUCCESS"
	return response, err
}

// contains : to check if array contains an item or not
func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

// MakeStorageID crerate a new storage ID
func MakeStorageID(owner string, sha []byte) string {
	shaStr := hex.EncodeToString(sha)
	res := sha256.Sum256([]byte(owner + "/" + shaStr))
	newSha := res[:]
	hexRes := make([]byte, hex.EncodedLen(len(newSha)))
	hex.Encode(hexRes, newSha)
	return string(hexRes)
}

// DecodeSha256HexString decode sha string
func DecodeSha256HexString(shaString string) (sha []byte, err error) {
	sha, err = hex.DecodeString(shaString)

	if err == nil && len(sha) != sha256.Size {
		err = errors.New("sha does not match expected length")
	}
	return
}
