//
// Copyright 2018-2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package models

import (
	"context"
	"errors"
	"log"
	"net/http"
	"os"
	"strconv"
	time "time"

	duration "github.com/ChannelMeter/iso8601duration"
	"github.com/gorilla/mux"
	"gitlab.com/pantacor/pantahub-gc/db"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"
)

// Device : Device model
type Device struct {
	ID               primitive.ObjectID     `json:"id" bson:"_id"`
	Prn              string                 `json:"prn"`
	Nick             string                 `json:"nick"`
	Owner            string                 `json:"owner"`
	OwnerNick        string                 `json:"owner-nick" bson:"-"`
	Secret           string                 `json:"secret,omitempty"`
	TimeCreated      time.Time              `json:"time-created" bson:"timecreated"`
	TimeModified     time.Time              `json:"time-modified" bson:"timemodified"`
	Challenge        string                 `json:"challenge,omitempty"`
	IsPublic         bool                   `json:"public"`
	UserMeta         map[string]interface{} `json:"user-meta" bson:"user-meta"`
	DeviceMeta       map[string]interface{} `json:"device-meta" bson:"device-meta"`
	Garbage          bool                   `json:"garbage" bson:"garbage"`
	GarbageRemovalAt time.Time              `bson:"garbage_removal_at" json:"garbage_removal_at"`
	GcProcessed      bool                   `json:"gc_processed" bson:"gc_processed"`
}

// ProcessDeviceGarbagesResponse : response of ProcessDeviceGarbages function
type ProcessDeviceGarbagesResponse struct {
	Status                int      `json:"status"`
	StatusMessage         string   `json:"status_message"`
	DeviceProcessed       int      `json:"device_processed"`
	TrailsMarkedAsGarbage int      `json:"trails_marked_as_garbage"`
	TrailsWithErrors      int      `json:"trails_with_errors"`
	Warnings              []string `json:"warnings"`
}

// MarkUnclaimedDevicesResponse : response of MarkUnClaimedDevicesAsGarbage() function
type MarkUnclaimedDevicesResponse struct {
	Status        int      `json:"status"`
	StatusMessage string   `json:"status_message"`
	DevicesMarked int      `json:"devices_marked"`
	Warnings      []string `json:"warnings"`
}

// MarkDeviceSummaryResponse : response of MarkDeviceSummaryGarbages() function
type MarkDeviceSummaryResponse struct {
	Status        int      `json:"status"`
	StatusMessage string   `json:"status_message"`
	DevicesMarked int      `json:"devices_marked"`
	Warnings      []string `json:"warnings"`
}

// DeleteGarbagesResponse : Response of  DeleteGarbages() function
type DeleteGarbagesResponse struct {
	Status        int                         `json:"status"`
	StatusMessage string                      `json:"status_message"`
	Devices       DeleteDevicesResponse       `json:"devices"`
	DeviceSummary DeleteDeviceSummaryResponse `json:"device_summary"`
	Trails        DeleteTrailsResponse        `json:"trails"`
	Steps         DeleteStepsResponse         `json:"steps"`
	Objects       DeleteObjectsResponse       `json:"objects"`
	Warnings      []string                    `json:"warnings"`
}

// DeleteDevicesResponse : response of deleteDeviceGarbage() function
type DeleteDevicesResponse struct {
	Status         int      `json:"status"`
	StatusMessage  string   `json:"status_message"`
	DevicesRemoved int      `json:"devices_removed"`
	Warnings       []string `json:"warnings"`
}

// DeleteDeviceSummaryResponse : response of deleteDeviceSummaryGarbage() function
type DeleteDeviceSummaryResponse struct {
	Status         int      `json:"status"`
	StatusMessage  string   `json:"status_message"`
	DevicesRemoved int      `json:"devices_removed"`
	Warnings       []string `json:"warnings"`
}

// MarkDeviceAsGarbage : Mark a device as garbage
func (device *Device) MarkDeviceAsGarbage() (
	result bool,
	err error,
) {
	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_devices")
	GarbageRemovalAt, err := GetGarbageRemovalTime()
	if err != nil {
		return false, errors.New("Error parsing garbage removal time:" + err.Error())
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	_, err = collection.UpdateOne(
		ctx,
		bson.M{"_id": device.ID},
		bson.M{"$set": bson.M{
			"garbage":            true,
			"garbage_removal_at": GarbageRemovalAt,
			"timemodified":       time.Now(),
		}},
	)
	if err != nil {
		return false, errors.New("Error marking device as garbage:" + err.Error() + "[ID:" + device.ID.Hex() + "]")
	}
	device.Garbage = true
	device.GarbageRemovalAt = GarbageRemovalAt
	return true, err
}

// MarkUnClaimedDevicesAsGarbage :  Mark all unclaimed devices as garbage after a while(eg: after 5 days)
func (device *Device) MarkUnClaimedDevicesAsGarbage() (
	response MarkUnclaimedDevicesResponse,
	err error,
) {
	response.DevicesMarked = 0

	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_devices")
	GarbageRemovalAt, err := GetGarbageRemovalTime()
	if err != nil {
		response.Status = 0
		response.StatusMessage = "FAIL"
		return response, err
	}
	TimeLeftForGarbaging := os.Getenv("PANTAHUB_GC_UNCLAIMED_EXPIRY")
	parsedDuration, err := duration.FromString(TimeLeftForGarbaging)
	if err != nil {
		response.Status = 0
		response.StatusMessage = "FAIL"
		return response, errors.New("Error parsing value of env:PANTAHUB_GC_UNCLAIMED_EXPIRY:" + err.Error() + "[ID:" + device.ID.Hex() + "]")
	}
	TimeBeforeDuration := time.Now().Local().Add(-parsedDuration.ToDuration())
	if os.Getenv("DEBUG") == "true" {
		log.Print("TimeBeforeDuration:")
		log.Print(TimeBeforeDuration)
		log.Print("GarbageRemovalAt:")
		log.Print(GarbageRemovalAt)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Hour)
	defer cancel()
	info, err := collection.UpdateMany(ctx,
		bson.M{
			"challenge":   bson.M{"$ne": ""},
			"timecreated": bson.M{"$lt": TimeBeforeDuration},
			"garbage":     bson.M{"$ne": true},
		},
		bson.M{"$set": bson.M{
			"garbage":            true,
			"garbage_removal_at": GarbageRemovalAt,
			"timemodified":       time.Now(),
		}},
	)

	if err != nil {
		response.Status = 0
		response.StatusMessage = "FAIL"
		if info != nil {
			response.DevicesMarked = int(info.ModifiedCount)
			return response, err
		}
		return response, errors.New("Error Marking unclaimed device as garbage:" + err.Error() + "[ID:" + device.ID.Hex() + "]")
	}
	response.Status = 1
	response.StatusMessage = "SUCCESS"
	response.DevicesMarked = int(info.ModifiedCount)
	return response, err
}

// ProcessDeviceGarbages : Process Device Garbages
func (device *Device) ProcessDeviceGarbages() (
	response ProcessDeviceGarbagesResponse,
	err error,
) {
	response.Status = 0
	response.StatusMessage = "FAIL"

	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_devices")
	ctx := context.Background()
	findOptions := options.Find()
	findOptions.SetNoCursorTimeout(true)
	//Fetch all device documents with (garbage:true AND (gc_processed:false if exist OR gc_processed not exist ))
	/* Note: Actual Record fetching will not happen here
	as it is using mongodb cursor and record fetching will
	start with we call cur.Next()
	*/
	cur, err := collection.Find(ctx, bson.M{
		"garbage":      true,
		"gc_processed": bson.M{"$ne": true},
	}, findOptions)
	if err != nil {
		return response, errors.New("Error fetching devices:" + err.Error())
	}
	if cur != nil {
		defer cur.Close(ctx)
	}
	response.DeviceProcessed = 0
	response.TrailsMarkedAsGarbage = 0
	response.TrailsWithErrors = 0
	for i := 0; cur != nil && cur.Next(ctx); i++ {
		err := cur.Err()
		if err != nil {
			return response, errors.New("Cursor error:" + err.Error())
		}
		device := Device{}
		err = cur.Decode(&device)
		if err != nil {
			response.Warnings = append(response.Warnings, "Cursor decode error:"+err.Error())
			continue
		}
		if os.Getenv("DEBUG") == "true" {
			log.Print("Processing Device:" + strconv.Itoa(i) + "[ID:" + device.ID.Hex() + "]")
		}

		warnings, err := markTrailAsGarbage(device.ID)
		if err != nil {
			response.TrailsWithErrors++
			response.Warnings = append(response.Warnings, "Error marking trail as garbage:"+err.Error())
		}
		if len(warnings) > 0 {
			response.Warnings = append(response.Warnings, warnings...)
			response.TrailsWithErrors++
		}

		if len(warnings) == 0 {
			response.TrailsMarkedAsGarbage++
		}

		//Marking device as gc_processed=true
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		updateResult, err := collection.UpdateOne(
			ctx,
			bson.M{"_id": device.ID},
			bson.M{"$set": bson.M{"gc_processed": true}},
		)
		if err != nil {
			if os.Getenv("DEBUG") == "true" {
				log.Print("Error Marking Device:" + strconv.Itoa(i) + " As Processed [ID:" + device.ID.Hex() + "]")
			}
			return response, errors.New("Error marking device as gc processed:" + err.Error() + "[ID:" + device.ID.Hex() + "]")
		}
		if updateResult.MatchedCount > 0 {
			response.DeviceProcessed++
			if os.Getenv("DEBUG") == "true" {
				log.Print("Marked Device:" + strconv.Itoa(i) + " As Processed [ID:" + device.ID.Hex() + "]")
			}
		}
	} //end for loop

	response.Status = 1
	response.StatusMessage = "SUCCESS"
	return response, err
}

// DeleteGarbages : Delete Garbages of a device
func (device *Device) DeleteGarbages() (
	response DeleteGarbagesResponse,
	err error,
) {
	response.Status = 0
	response.StatusMessage = "FAIL"

	if os.Getenv("PANTAHUB_GC_REMOVE_GARBAGE") == "true" {

		//Enabled
		// 1.Delete Devices
		response.Devices, err = deleteDeviceGarbage()
		if err != nil {
			return response, err
		}
		// 2.Delete Device Summary
		response.DeviceSummary, err = deleteDeviceSummaryGarbage()
		if err != nil {
			return response, err
		}
		// 3.Delete Trails
		response.Trails, err = deleteTrailGarbage()
		if err != nil {
			return response, err
		}
		// 4.Delete Steps
		response.Steps, err = deleteStepsGarbage()
		if err != nil {
			return response, err
		}
		// 5.Delete Objects
		response.Objects, err = deleteAllObjectsGarbage()
		if err != nil {
			return response, err
		}
		response.Status = 1
		response.StatusMessage = "SUCCESS"
	} else {
		//Disabled
		response.Status = 1
		response.StatusMessage = "DISABLED"

		response.Devices = DeleteDevicesResponse{
			Status:        1,
			StatusMessage: "DISABLED",
			Warnings:      []string{},
		}
		response.DeviceSummary = DeleteDeviceSummaryResponse{
			Status:        1,
			StatusMessage: "DISABLED",
			Warnings:      []string{},
		}
		response.Trails = DeleteTrailsResponse{
			Status:        1,
			StatusMessage: "DISABLED",
			Warnings:      []string{},
		}
		response.Steps = DeleteStepsResponse{
			Status:        1,
			StatusMessage: "DISABLED",
			Warnings:      []string{},
		}

		response.Objects = DeleteObjectsResponse{
			Status:        1,
			StatusMessage: "DISABLED",
			Warnings:      []string{},
		}
	}
	return response, err
}

// Validate : Validate PUT devices/{id} request
func (device *Device) Validate(r *http.Request) error {
	params := mux.Vars(r)
	deviceID, err := primitive.ObjectIDFromHex(params["id"])
	if err != nil {
		return errors.New("Invalid Document ID" + "[ID:" + params["id"] + "]")
	}
	device.ID = deviceID
	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_devices")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	err = collection.FindOne(ctx,
		bson.M{"_id": device.ID}).
		Decode(&device)
	if err != nil {
		return errors.New("Error finding device:" + err.Error() + "[ID:" + device.ID.Hex() + "]")
	}
	return nil
}

// GetGarbageRemovalTime : Get Garbage Removal Time
func GetGarbageRemovalTime() (time.Time, error) {
	RemoveGarbageAfter := os.Getenv("PANTAHUB_GC_GARBAGE_EXPIRY")
	parsedDuration, err := duration.FromString(RemoveGarbageAfter)
	GarbageRemovalAt := time.Now().Local().Add(parsedDuration.ToDuration())
	if err != nil {
		return GarbageRemovalAt, errors.New("Error parsing garbage expiry time:" + err.Error())
	}
	return GarbageRemovalAt, nil
}

// IsDeviceValid : to check if a DeviceID is valid or not
func IsDeviceValid(DeviceID primitive.ObjectID) (
	result bool,
	err error,
) {
	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_devices")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	count, err := collection.CountDocuments(ctx, bson.M{
		"_id": DeviceID,
	})
	return (count == 1), err
}

// MarkDeviceSummaryGarbages : Mark Deleted Devices As Garbage In DeviceSummary
func (device *Device) MarkDeviceSummaryGarbages() (
	response MarkDeviceSummaryResponse,
	err error,
) {
	response.Status = 0
	response.StatusMessage = "FAIL"
	response.DevicesMarked = 0

	devicesWithErrors := []string{}

	collection := db.Client().Database(db.GetDevSummaryDb()).Collection("device_summary_short_new_v2")
	ctx := context.Background()
	findOptions := options.Find()
	findOptions.SetNoCursorTimeout(true)
	/* Note: Actual Record fetching will not happen here
	as it is using mongodb cursor and record fetching will
	start with we call cur.Next() */
	// Create mongo cursor
	cur, err := collection.Find(ctx, bson.M{
		"garbage": bson.M{"$ne": true},
	}, findOptions)
	if err != nil {
		return response, errors.New("Error fetcing device summary:" + err.Error())
	}
	if cur != nil {
		defer cur.Close(ctx)
	}
	for cur != nil && cur.Next(ctx) {
		err := cur.Err()
		if err != nil {
			return response, errors.New("Cursor error:" + err.Error())
		}
		result := map[string]interface{}{}
		err = cur.Decode(&result)
		if err != nil {
			response.Warnings = append(response.Warnings, "Cursor decode error:"+err.Error())
			continue
		}
		deviceID := result["deviceid"].(string)
		if contains(devicesWithErrors, deviceID) {
			continue
		}
		deviceObjectID, err := primitive.ObjectIDFromHex(result["deviceid"].(string))
		if err != nil {
			response.Warnings = append(response.Warnings, "invalid_hex:"+err.Error())
			continue
		}
		deviceResult, err := IsDeviceValid(deviceObjectID)
		if err != nil {
			response.Warnings = append(response.Warnings, err.Error())
			devicesWithErrors = append(devicesWithErrors, deviceID)
		}
		if !deviceResult {
			err := markDeviceSummaryAsGarbage(deviceID)
			if err != nil {
				response.Warnings = append(response.Warnings, err.Error())
				continue
			}
			response.DevicesMarked++
		}
	}

	if err := cur.Err(); err != nil {
		response.Warnings = append(response.Warnings, "cursor error:"+err.Error())
	}

	response.Status = 1
	response.StatusMessage = "SUCCESS"
	return response, err
}

// markDeviceSummaryAsGarbage : Mark Device Summary As Garbage
func markDeviceSummaryAsGarbage(deviceID string) (
	err error,
) {
	GarbageRemovalAt, err := GetGarbageRemovalTime()
	if err != nil {
		return err
	}
	collection := db.Client().Database(db.GetDevSummaryDb()).Collection("device_summary_short_new_v2")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_, err = collection.UpdateOne(
		ctx,
		bson.M{"deviceid": deviceID},
		bson.M{"$set": bson.M{
			"garbage":            true,
			"garbage_removal_at": GarbageRemovalAt},
			"timemodified": time.Now(),
		},
	)
	if err != nil {
		return errors.New("Error marking device summary as garbage:" + err.Error() + "[Device ID:" + deviceID + "]")
	}
	return nil
}

// deleteDeviceGarbage : Delete Device Garbages
func deleteDeviceGarbage() (
	response DeleteDevicesResponse,
	err error,
) {
	response.Status = 0
	response.StatusMessage = "FAIL"
	response.DevicesRemoved = 0

	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_devices")
	now := time.Now().Local()
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	info, err := collection.DeleteMany(ctx,
		bson.M{
			"garbage":            true,
			"garbage_removal_at": bson.M{"$lt": now},
		})
	if err != nil {
		if info != nil {
			response.DevicesRemoved = int(info.DeletedCount)
		}
		return response, errors.New("Error removing garbage devices:" + err.Error())
	}
	response.Status = 1
	response.StatusMessage = "SUCCESS"
	response.DevicesRemoved = int(info.DeletedCount)
	return response, err
}

// deleteDeviceSummaryGarbage : Delete Device Summary Garbages
func deleteDeviceSummaryGarbage() (
	response DeleteDeviceSummaryResponse,
	err error,
) {
	response.Status = 0
	response.StatusMessage = "FAIL"
	response.DevicesRemoved = 0

	collection := db.Client().Database(db.GetDevSummaryDb()).Collection("evice_summary_short_new_v2")
	now := time.Now().Local()
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	info, err := collection.DeleteMany(ctx,
		bson.M{"garbage": true,
			"garbage_removal_at": bson.M{"$lt": now},
		})
	if err != nil {
		if info != nil {
			response.DevicesRemoved = int(info.DeletedCount)
		}
		return response, errors.New("Error removing device summary garbages:" + err.Error())
	}
	response.Status = 1
	response.StatusMessage = "SUCCESS"
	response.DevicesRemoved = int(info.DeletedCount)
	return response, err
}
